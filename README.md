# scribus-ci-docker

CI base image for Scribus builds

## Local testing

- (install the `docker.io` package) (add yourself to the docker group; `newgrp docker`)
- list the installed images: `docker images`
- `docker pull registry.gitlab.com/scribus/scribus-ci-docker/main`
- `docker run -i -t registry.gitlab.com/scribus/scribus-ci-docker/main /bin/bash`
- `git clone --depth 1 https://gitlab.com/scribus/scribus.git`
- `cd scribus`
- run the commands in `Appimage/bundle.sh`
  - TODO: try to copy in the `bundle.sh` script.

Copying the file out of the running docker image:

- `docker ps` to get the container id
- `docker cp <containerId>:/scribus/Scribus-nightly-x86_64.AppImage /tmp`

Adding a custom `AppRun` and `bundle.sh` image in the docker image:

- `docker cp <Scribus sources>/AppImage-package/AppRun <containerId>:/scribus/AppImage-package`
- `docker cp <Scribus sources>/AppImage-package/bundle.sh <containerId>:/scribus/AppImage-package`

## Rebuilding the docker image

The docker image is built on each push of the `Dockerfile`.

One can regenerate it by clicking on the "retry" button on the last job in the Gitlab CI/CD

## Cleaning up

- Stop all the containers:  
  `docker stop $(docker ps -a -q)`
- Remove all the containers:  
  `docker rm $(docker ps -a -q)`
- Remove images:  
  `docker images`  
  `docker rmi <image name>`
