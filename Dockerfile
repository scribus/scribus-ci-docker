FROM ubuntu:24.04
RUN apt-get update -yqq
RUN export DEBIAN_FRONTEND=noninteractive && \
apt-get update -yqq && \
apt-get install -y software-properties-common && \
apt-get update -yqq && \
apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages \
cmake libboost-dev libcups2-dev libhunspell-dev \
libhyphen-dev liblcms2-dev libpodofo-dev \
libpng-dev libjpeg-dev libtiff-dev \
libpython3-dev  python3-tk \
zlib1g-dev \
libxml2-dev \
qt6-base-dev qt6-base-private-dev qt6-declarative-dev \
qt6-tools-dev qt6-wayland-dev libqt6core5compat6-dev qt6-svg-dev \
libqt6svg6-dev linguist-qt6 \
qt6-base-dev-tools qt6-image-formats-plugins qt6-l10n-tools qt6-tools-dev-tools qt6-translations-l10n \
qt6-gtk-platformtheme \
libgraphicsmagick++1-dev \
libopenscenegraph-dev libpoppler-dev libpoppler-cpp-dev \
libpoppler-qt6-dev libpoppler-dev libpoppler-private-dev \
libcairo2-dev libfreetype6-dev \
libwpg-dev \
libmspub-dev libcdr-dev libvisio-dev libharfbuzz-dev libharfbuzz-icu0 \
coreutils binutils wget ccache patchelf git \
libgtk2.0-dev  \
&& rm -rf /var/lib/apt/lists/*
